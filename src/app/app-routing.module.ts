import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotMatchComponent } from './not-match/not-match.component';
import { CustomerComponent } from './customer/customer.component';
import { CurrencyComponent } from './currency/currency.component';
import { OperationComponent } from './operation/operation.component';
import { OperationTypeComponent } from './operation-type/operation-type.component';
import { PocketComponent } from './pocket/pocket.component';
import { CreateCustomerComponent } from './customer/create-customer/create-customer.component';
import { CreateCurencyComponent } from './currency/create-curency/create-curency.component';
import { OperationCreateComponent } from './operation/operation-create/operation-create.component';
import { PocketCreateComponent } from './pocket/pocket-create/pocket-create.component';


const routes: Routes = [
    { 
        path: 'customers', 
        component: CustomerComponent
    },
    {
        path: 'customer/add',
        component: CreateCustomerComponent
    },
    {
        path: 'customer/:id',
        component: CreateCustomerComponent
    },
    {
        path: 'currency',
        component: CurrencyComponent
    },
    {
        path: 'currency/add',
        component: CreateCurencyComponent
    },
    {
        path: 'currency/:id',
        component: CreateCurencyComponent
    },
    {
        path: 'operation',
        component: OperationComponent
    },
    {
        path: 'operation/add',
        component: OperationCreateComponent
    },
    {
        path: 'operation/:id',
        component: OperationCreateComponent
    },
    {
        path: 'operationtype',
        component: OperationTypeComponent
    },
    {
        path: 'pocket',
        component: PocketComponent
    },
    {
        path: 'pocket/add',
        component: PocketCreateComponent
    },
    {
        path: 'pocket/:id',
        component: PocketCreateComponent
    },
    {
        path: '',
        component: DashboardComponent,
        pathMatch: 'full'
    },
    {
        path: '**',
        component: NotMatchComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
