import { Injectable } from '@angular/core';
import { Customer } from '../entity/customer';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { OperationType } from '../entity/operation-type';
import { Currency } from '../entity/currency';
import { Pocket } from '../entity/pocket';
import { Operation } from '../entity/operation';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    private BASE_URL: string = 'http://localhost:8080/';
    private LIST_SUFFIX: string = 'List';
    private CUSTOMER_URL: string = `${this.BASE_URL}\\customer`;
    private OPERATION_TYPE_URL: string = `${this.BASE_URL}\\operationtype`;
    private OPERATION_URL: string = `${this.BASE_URL}\\operation`;
    private POCKET_URL: string = `${this.BASE_URL}\\pocket`;
    private CURRENCY_URL: string = `${this.BASE_URL}\\currency`;


    constructor(private http: HttpClient) {
    }

    public getCustomerList(): Observable<Customer[]> {
        return this.http.get<Customer[]>(`${this.CUSTOMER_URL}${this.LIST_SUFFIX}`);
    }

    public saveCustomer(customer: Customer) {
        return this.http.post<Customer>(this.CUSTOMER_URL, customer);
    }

    public getCustomerById(customerId: string): Observable<Customer> {
        return this.http.get<Customer>(`${this.CUSTOMER_URL}\\${customerId}`);
    }

    public deleteCustomerById(customerId: string) {
        return this.http.delete<Customer>(`${this.CUSTOMER_URL}\\${customerId}`);
    }

    public getOperationTypeList(): Observable<OperationType> {
        return this.http.get<OperationType>(`${this.OPERATION_TYPE_URL}${this.LIST_SUFFIX}`);
    }

    public saveOperationType(operationType: OperationType) {
        return this.http.post<OperationType>(this.OPERATION_TYPE_URL, operationType);
    }

    public getOperationList(): Observable<Operation[]> {
        return this.http.get<Operation[]>(`${this.OPERATION_URL}${this.LIST_SUFFIX}`);
    }

    public saveOperation(operation: Operation) {
        return this.http.post<Operation>(this.OPERATION_URL, operation);
    }

    public getOperationById(operationId: string): Observable<Operation> {
        return this.http.get<Operation>(`${this.OPERATION_URL}\\${operationId}`);
    }

    public deleteOperationById(operationId: string) {
        return this.http.delete<Operation>(`${this.OPERATION_URL}\\${operationId}`);
    }

    public getPocketList(): Observable<Pocket[]> {
        return this.http.get<Pocket[]>(`${this.POCKET_URL}${this.LIST_SUFFIX}`);
    }

    public getPocketById(pocketId: string): Observable<Pocket> {
        return this.http.get<Pocket>(`${this.POCKET_URL}\\${pocketId}`);
    }

    public deletePocket(pocketId: string) {
        return this.http.delete<Pocket>(`${this.POCKET_URL}\\${pocketId}`);
    }

    public savePocket(pocket: Pocket) {
        return this.http.post<Pocket>(this.POCKET_URL, pocket);
    }

    public getCurrencyList(): Observable<Currency[]> {
        return this.http.get<Currency[]>(`${this.CURRENCY_URL}${this.LIST_SUFFIX}`);
    }

    public getCurrencyById(currencyId: string): Observable<Currency> {
        return this.http.get<Currency>(`${this.CURRENCY_URL}\\${currencyId}`);
    }

    public saveCurrency(currency: Currency) {
        return this.http.post<Currency>(this.CURRENCY_URL, currency);
    }

    public deleteCurrencyById(currencyId: string) {
        return this.http.delete<Currency>(`${this.CURRENCY_URL}\\${currencyId}`);
    }
}
