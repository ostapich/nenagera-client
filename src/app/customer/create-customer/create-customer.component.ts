import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/entity/customer';
import { ApiService } from 'src/app/service/api.service';
import { ParamMap, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-create-customer',
    templateUrl: './create-customer.component.html',
    styleUrls: ['./create-customer.component.scss']
})
export class CreateCustomerComponent implements OnInit {

    model: Customer = {
        id: '',
        name: '',
        password: ''
    };

    constructor(
        private apiClient: ApiService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        // let id: string = this.params.get('id');
        let id: string = this.route.snapshot.params.id;
        if (id != null) {
            this.apiClient.getCustomerById(id).subscribe(
                customer => {
                    this.model = customer;
                },
                err => {
                    alert('Customer is not loaded');
                }
            );
        }
    }

    public saveCustomer(): void {
        this.apiClient.saveCustomer(this.model).subscribe(
            customer => {
                location.href = '/customers';
            },
            err => {
                alert('error');
            }
        );
    }

}
