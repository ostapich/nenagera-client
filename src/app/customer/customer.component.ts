import { Component, OnInit } from '@angular/core';
import { Customer } from '../entity/customer';
import { ApiService } from '../service/api.service';

@Component({
    selector: 'app-customer',
    templateUrl: './customer.component.html',
    styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

    customers: Customer[];

    constructor(private apiService: ApiService) { }

    ngOnInit() {
        this.apiService.getCustomerList().subscribe(data => {
            this.customers = data['content'];
        });
    }

    editCustomer(user: Customer) {
        location.href = '/customer/' + user.id;
    }

    deleteCustomer(user: Customer) {
        this.apiService.deleteCustomerById(user.id).subscribe(
            success => {
                let indexOfUser = this.customers.indexOf(user);
                this.customers.splice(indexOfUser, 1);
            },
            err => {

            }
        );
    }
}
