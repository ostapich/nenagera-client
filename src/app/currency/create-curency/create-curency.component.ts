import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ActivatedRoute } from '@angular/router';
import { Currency } from 'src/app/entity/currency';

@Component({
    selector: 'app-create-curency',
    templateUrl: './create-curency.component.html',
    styleUrls: ['./create-curency.component.scss']
})
export class CreateCurencyComponent implements OnInit {

    model: Currency = {
        id: '',
        name: ''
    }

    constructor(
        private apiClient: ApiService,
        private route: ActivatedRoute 
    ) { }

    ngOnInit() {
        let currencyId: string = this.route.snapshot.params.id;

        if (currencyId != null) {
            this.apiClient.getCurrencyById(currencyId).subscribe(
                data => {
                    this.model = data;
                },
                error => {
                    alert('Currency is not found');
                }
            );
        }
    }

    public saveCurrency() {
        this.apiClient.saveCurrency(this.model).subscribe(
            success => {
                location.href = '/currency'
            },
            error => {
                alert('Error is happened during the saving');
            }
        )
    }

}
