import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCurencyComponent } from './create-curency.component';

describe('CreateCurencyComponent', () => {
  let component: CreateCurencyComponent;
  let fixture: ComponentFixture<CreateCurencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCurencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCurencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
