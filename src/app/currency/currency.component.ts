import { Component, OnInit } from '@angular/core';
import { Currency } from '../entity/currency';
import { ApiService } from '../service/api.service';

@Component({
    selector: 'app-currency',
    templateUrl: './currency.component.html',
    styleUrls: ['./currency.component.scss']
})
export class CurrencyComponent implements OnInit {

    currencies: Currency[];

    constructor(private apiClient: ApiService) { }

    ngOnInit() {
        this.apiClient.getCurrencyList().subscribe(
            currencyList => {
                this.currencies = currencyList['content'];
            },
            err => {
                alert('Error is hapened');
            }
        );
    }

    public editCurrency(currency: Currency) {
        location.href = '/currency/' + currency.id;
    }

    public deleteCurrency(currency: Currency) {
        this.apiClient.deleteCurrencyById(currency.id).subscribe(
            success => {
                let indexOfCurrencies = this.currencies.indexOf(currency);
                this.currencies.splice(indexOfCurrencies, 1);
            }
        )
    }

}
