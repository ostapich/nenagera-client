import { Component, OnInit } from '@angular/core';
import { Operation } from '../entity/operation';
import { Big } from 'big.js/big';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.scss']
})
export class OperationComponent implements OnInit {

    operations: Operation[];

    constructor(private apiClient: ApiService) { }

    ngOnInit() {
        this.apiClient.getOperationList().subscribe(
            data => {
                console.log(data);
                this.operations = data['content'];
            },
            err => {
                alert('Operations are not loaded');
            }
        );
    }

    public editOperation(operation: Operation) {
        location.href = '/operation/' + operation.id;
    }

    public deleteOperation(operation: Operation) {
        this.apiClient.deleteOperationById(operation.id).subscribe(
            success => {
                let indexOfOperation = this.operations.indexOf(operation);
                this.operations.splice(indexOfOperation, 1);
            },
            err => {
                alert('Could not removed');
            }
        );

    }
  
}
