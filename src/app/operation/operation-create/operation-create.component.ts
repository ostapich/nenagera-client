import { Component, OnInit } from '@angular/core';
import { Operation } from 'src/app/entity/operation';
import { Big } from 'big.js/big';
import { ApiService } from 'src/app/service/api.service';
import { OperationType } from 'src/app/entity/operation-type';
import { Customer } from 'src/app/entity/customer';
import { Pocket } from 'src/app/entity/pocket';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-operation-create',
    templateUrl: './operation-create.component.html',
    styleUrls: ['./operation-create.component.scss']
})
export class OperationCreateComponent implements OnInit {

    model: Operation = {
        id: null,
        description: '',
        amount: new Big(0.0),
        operationType: '',
        pocket: '',
        customer: ''
    };

    operationTypes: OperationType[];

    customers: Customer[];

    pockets: Pocket[];

    constructor(
        private apiClient: ApiService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        let operationId: string = this.route.snapshot.params.id;

        if (operationId != null) {
            this.apiClient.getOperationById(operationId).subscribe(
                operation => {
                    this.model = operation;
                    this.model.operationType = operation['operationType']['id'];
                },
                err => {
                    alert('Operation is not load');
                }
            );
        }
        
        this.apiClient.getOperationTypeList().subscribe(data => {
            this.operationTypes = data['content'];
        });

        this.apiClient.getPocketList().subscribe(
            data => {
                this.pockets = data['content'];
            },
            err => {
                alert('Pockets are not loaded');
            }
        );
        
        this.apiClient.getCustomerList().subscribe(
            data => {
                this.customers = data['content'];
            },
            err => {
                alert('Customers are not loaded');
            }
        );
    }

    saveOperation(): void {
        // console.log(this.model);
        // if (this.model.operationType['id'] != null) {
        //     this.model.operationType = this.model.operationType['id'];
        // }

        // if ()
        this.apiClient.saveOperation(this.model).subscribe(
            res => {
                location.href = '/operation';
            },
            err => {
                alert("error happened");
            }
        );
    }

}
