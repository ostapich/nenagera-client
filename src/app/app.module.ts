import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { NavigationComponent } from './navigation/navigation.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotMatchComponent } from './not-match/not-match.component';
import { CurrencyComponent } from './currency/currency.component';
import { OperationComponent } from './operation/operation.component';
import { PocketComponent } from './pocket/pocket.component';
import { OperationTypeComponent } from './operation-type/operation-type.component';
import { CustomerComponent } from './customer/customer.component';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';
import { CreateCustomerComponent } from './customer/create-customer/create-customer.component';
import { CreateCurencyComponent } from './currency/create-curency/create-curency.component';
import { OperationCreateComponent } from './operation/operation-create/operation-create.component';
import { PocketCreateComponent } from './pocket/pocket-create/pocket-create.component';
import { OperationTypeCreateComponent } from './operation-type/operation-type-create/operation-type-create.component';

@NgModule({
    declarations: [
        AppComponent,
        NavigationComponent,
        DashboardComponent,
        NotMatchComponent,
        CurrencyComponent,
        OperationComponent,
        PocketComponent,
        OperationTypeComponent,
        CustomerComponent,
        FooterComponent,
        CreateCustomerComponent,
        CreateCurencyComponent,
        OperationCreateComponent,
        PocketCreateComponent,
        OperationTypeCreateComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FontAwesomeModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
