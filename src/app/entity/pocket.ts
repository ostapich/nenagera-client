export class Pocket {
    id: string;
    name: string;
    currency: string;
}
