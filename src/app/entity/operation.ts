import { Big } from 'big.js/big';
import { OperationType } from './operation-type';

export class Operation {
    id: string;
    description: string;
    amount: Big;
    operationType: string;
    pocket: string;
    customer: string;

}
