import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationTypeCreateComponent } from './operation-type-create.component';

describe('OperationTypeCreateComponent', () => {
  let component: OperationTypeCreateComponent;
  let fixture: ComponentFixture<OperationTypeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationTypeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationTypeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
