import { Component, OnInit } from '@angular/core';
import { Pocket } from '../entity/pocket';
import { ApiService } from '../service/api.service';
import { Currency } from '../entity/currency';

@Component({
  selector: 'app-pocket',
  templateUrl: './pocket.component.html',
  styleUrls: ['./pocket.component.scss']
})
export class PocketComponent implements OnInit {
    
    pockets: Pocket[];
    currency: Currency;

    constructor( private apiClient: ApiService ) { }

    ngOnInit() {
        this.apiClient.getPocketList().subscribe(
            data => {
                console.log(data);
                this.pockets = data['content']
            },
            err => {
                alert('Content is not loaded');
            }
        );
    }

    public editPocket(pocket: Pocket) {
        location.href = '/pocket/' + pocket.id;
    }

    public deletePocket(pocket: Pocket) {
        this.apiClient.deletePocket(pocket.id).subscribe(
            data => {
                let indexOfPockets = this.pockets.indexOf(pocket);
                this.pockets.splice(indexOfPockets, 1);
            },
            err => {
                alert('Pocket is not deleted');
            }
        );
    }

}
