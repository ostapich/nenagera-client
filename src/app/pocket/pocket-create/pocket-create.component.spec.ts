import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PocketCreateComponent } from './pocket-create.component';

describe('PocketCreateComponent', () => {
  let component: PocketCreateComponent;
  let fixture: ComponentFixture<PocketCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PocketCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PocketCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
