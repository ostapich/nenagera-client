import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ActivatedRoute } from '@angular/router';
import { Pocket } from 'src/app/entity/pocket';
import { Currency } from 'src/app/entity/currency';

@Component({
    selector: 'app-pocket-create',
    templateUrl: './pocket-create.component.html',
    styleUrls: ['./pocket-create.component.scss']
})
export class PocketCreateComponent implements OnInit {

    model: Pocket = {
        id: '',
        name: '',
        currency: ''

    };

    currencies: Currency[];

    constructor(
        private apiClient: ApiService,
        private route: ActivatedRoute 
    ) { }

    ngOnInit() {
        let pocketId = this.route.snapshot.params.id;

        if (pocketId != null) {
            this.apiClient.getPocketById(pocketId).subscribe(
                data => {
                    this.model = data;
                    this.model.currency = data['currency']['id'];
                },
                error => {
                    alert('Currency is not found');
                }
            );
        }

        this.apiClient.getCurrencyList().subscribe(
            data => {
                this.currencies = data['content'];
            },
            error => {
                alert('No currency')
            }
        );
    }

    public savePocket() {
        // if (this.model.currency['id'] != null) {
        //     this.model.currency = this.model.currency['id'];
        // }
        console.log(this.model);
        this.apiClient.savePocket(this.model).subscribe(
            data => {
                location.href = '/pocket';
            },
            error => {
                alert('error is happened during the pocket saving');
            }
        );
    }

    public changeCurrency(currency) {
        console.log(currency);
    }
}
